## My Magic Prompt

My Magic Prompt is a project in My Digital School for learn and train bash script.

## Instalation

Become main.sh a executable file with : `chmod +x main.sh`

### Package needed

* curl
* vim
* mailutils

### Start prompt

Do `./main.sh` for start the prompt

## Available Scripts

When the prompt started, you can run:

## Available Command in prompt

### `about`

Description of the program

### `age`

Enter your age and the program will tell you if your an adult or a minor

### `cd`

Move into a directory or create it if is not exist

### `help`

List all command

### `hour`

Give you current hour

### `httpget`

Target a web page and download the html code in a file

### `ls`

List information about the files in the current directory

### `open`

Open a file in VIM editor, create the file if is dont exist

### `passw`

Change your password (confirmation necessary)

### `profile`

Information about me

### `pwd`

PATH of your current directory

### `quit` or `exit`

This command stop the prompt

### `rm` or `rmd` or `rmdir`

Removes specified files or directory

### `smtp`

Send an email with a subject and body

### `version` or `--v` or `vers`

This command is used to print out version information